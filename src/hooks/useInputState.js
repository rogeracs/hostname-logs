import React, { useState } from "react";

const useInputState = callback => {
  const [inputs, setInputs] = useState({});

  const handleSubmit = event => {
    if (event) {
      event.preventDefault();
      //   console.log(inputs);
      addRecord(inputs);
    }
    callback();
  };

  const handleInputChange = event => {
    event.persist();
    setInputs(inputs => ({
      ...inputs,
      [event.target.name]: event.target.value
    }));
  };

  const reset = () => {
    setInputs("");
  };

  return {
    handleSubmit,
    handleInputChange,
    inputs,
    reset
  };
};

export default useInputState;
