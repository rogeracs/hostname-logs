import React, { useState } from "react";
import "./app.scss";
import uuid from "uuid";
import orderBy from "lodash/orderBy";
import HostnameForm from "./HostnameForms";
import HostnameTable from "./HostnameTable";
import EditUserForm from "./EditUserForm";
import Paper from "@material-ui/core/Paper";

export default function App() {
  const [record, setRecord] = useState([
    {
      id: uuid(),
      hostname: "tdg-mbl-mty200",
      serviceTag: "4RN9CL",
      date: "1/1/2019",
      user: "Rogelio Camacho",
      columnToSort: "",
      sortDirection: "asc"
    },
    {
      id: uuid(),
      hostname: "tdg-mbl-mty201",
      serviceTag: "5RN8CL",
      date: "1/1/2019",
      user: "Jorge Santos",
      columnToSort: "",
      sortDirection: "asc"
    },
    {
      id: uuid(),
      hostname: "tdg-mbl-mty202",
      serviceTag: "2RNFCD",
      date: "1/1/2019",
      user: "Jhon Salchichon",
      columnToSort: "",
      sortDirection: "asc"
    }
  ]);

  const header = [
    { name: "Hostname", prop: "hostname" },
    { name: "Service Tag", prop: "service tag" },
    { name: "Date", prop: "date" },
    { name: "User", prop: "user" },
    { name: "", prop: "" },
    { name: "", prop: "" }
  ];

  // const initialState = { columnToSort: "", sortDirection: "desc" };

  // const [state, setState] = useState(initialState);

  const [editing, setEditing] = useState(false);

  const initialFormState = {
    id: null,
    hostname: "",
    serviceTag: "",
    date: "",
    user: " "
  };

  const [currentRecord, setCurrentRecord] = useState(initialFormState);

  const editRow = record => {
    setEditing(true);
    setCurrentRecord({
      id: record.id,
      hostname: record.hostname,
      serviceTag: record.serviceTag,
      date: record.date,
      user: record.user
    });
  };

  const deleteRecord = recordID => {
    const updated = record.filter(log => log.id !== recordID);

    setRecord(updated);
  };

  const updateRecord = (id, updateRecord) => {
    setEditing(false);

    setRecord(record.map(record => (record.id === id ? updateRecord : record)));
  };

  const invertDirection = {
    asc: "desc",
    desc: "asc"
  };

  const handleSort = columnName => {
    // const newSort = {
    //   columnToSort: columnName,
    //   sortDirection:
    //     record.columnToSort === columnName
    //       ? invertDirection[record.sortDirection]
    //       : "asc"
    // };
    // console.log(newSort);
    // setRecord(newSort);

    setRecord(record => ({
      columnToSort: columnName,
      sortDirection:
        record.columnToSort === columnName
          ? invertDirection[record.sortDirection]
          : "asc"
    }));
  };

  return (
    <div>
      <Paper>
        <h1>Hostname Log</h1>

        <HostnameTable
          record={orderBy(record, record.columnToSort, record.sortDirection)}
          header={header}
          deleteRecord={deleteRecord}
          setRecord={setRecord}
          id={record.id}
          editRow={editRow}
          handleSort={handleSort}
        />

        {editing ? (
          <>
            <h2>Edit user</h2>
            <EditUserForm
              editing={editing}
              setEditing={setEditing}
              currentRecord={currentRecord}
              updateRecord={updateRecord}
              setRecord={setRecord}
              record={record}
            />
          </>
        ) : (
          <HostnameForm
            record={record}
            setRecord={setRecord}
            deleteRecord={deleteRecord}
          />
        )}
      </Paper>
    </div>
  );
}
