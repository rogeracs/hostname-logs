import React, { useState } from "react";
import uuid from "uuid";
import useToggle from "./hooks/useToggle";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  dense: {
    marginTop: 19
  },
  menu: {
    width: 200
  }
}));

// ----------------------------------------------------------------------------------------------------------------------

export default function HostnameForms({ record, setRecord, deleteRecord }) {
  const classes = useStyles();
  // ----------------------------------------------------------------------------------------------------------------------

  // ----------------------------------------------------------------------------------------------------------------------
  const [input, setInput] = useState({
    id: "",
    hostname: "",
    serviceTag: "",
    date: "",
    user: ""
  });

  const handleChange = event => {
    setInput({ ...input, [event.target.name]: event.target.value });
  };

  const handleSubmit = event => {
    event.preventDefault();
    if (
      input.hostname.trim() === "" ||
      input.serviceTag.trim() === "" ||
      input.date.trim() === "" ||
      input.user.trim() === ""
    )
      return;
    const newLog = {
      id: uuid(),
      hostname: input.hostname.trim(),
      serviceTag: input.serviceTag.trim(),
      date: input.date.trim(),
      user: input.user.trim()
    };

    setRecord([...record, newLog]);
    setInput({ id: "", hostname: "", serviceTag: "", date: "", user: "" });
  };

  // -----------------------------------------------------------------------------------------------------------------

  return (
    <div>
      <form>
        <TextField
          label="Hostname"
          className={classes.textField}
          name="hostname"
          value={input.hostname}
          onChange={handleChange}
          margin="normal"
        />

        <TextField
          label="Service Tag"
          className={classes.textField}
          name="serviceTag"
          value={input.serviceTag}
          onChange={handleChange}
          margin="normal"
        />

        <TextField
          label="Date"
          className={classes.textField}
          name="date"
          value={input.date}
          onChange={handleChange}
          margin="normal"
        />
        <TextField
          label="User"
          className={classes.textField}
          name="user"
          value={input.user}
          onChange={handleChange}
          margin="normal"
        />

        <Button
          style={{ marginTop: "20px", marginLeft: "20px" }}
          variant="contained"
          color="primary"
          onClick={handleSubmit}
        >
          Add Record
        </Button>
      </form>
    </div>
  );
}
