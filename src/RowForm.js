import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  dense: {
    marginTop: 19
  },
  menu: {
    width: 200
  }
}));

export default function RowForm({ input, handleChange, handleSubmit }) {
  const classes = useStyles();
  return (
    <div>
      <form>
        <TextField
          name="hostname"
          className={classes.textField}
          value={input.hostname}
          onChange={handleChange}
          margin="normal"
        />

        <TextField
          name="serviceTag"
          className={classes.textField}
          value={input.serviceTag}
          onChange={handleChange}
        />

        <TextField
          name="date"
          className={classes.textField}
          value={input.date}
          onChange={handleChange}
        />
        <TextField
          name="user"
          className={classes.textField}
          value={input.user}
          onChange={handleChange}
        />
      </form>
    </div>
  );
}
