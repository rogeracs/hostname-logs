import React, { useState } from "react";
import uuid from "uuid";

import TableCell from "@material-ui/core/TableCell";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

function EditForm({
  id,
  hostname,
  serviceTag,
  date,
  user,
  changeEditMode,

  setRecord,
  record,
  handleToggle
}) {
  const [input, setInput] = useState({
    id,
    hostname,
    serviceTag,
    date,
    user
  });

  const handleChange = event => {
    setInput({ ...input, [event.target.name]: event.target.value });
  };

  const handleSubmit = event => {
    event.preventDefault();
    if (
      input.hostname.trim() === "" ||
      input.serviceTag.trim() === "" ||
      input.date.trim() === "" ||
      input.user.trim() === ""
    )
      return;
    const newLog = {
      id: uuid(),
      hostname: input.hostname.trim(),
      serviceTag: input.serviceTag.trim(),
      date: input.date.trim(),
      user: input.user.trim()
    };

    setRecord([...record, newLog]);
  };

  return (
    <>
      <TableCell>
        <TextField
          name="hostname"
          value={input.hostname}
          onChange={handleChange}
        />
      </TableCell>
      <TableCell>
        {" "}
        <TextField
          name="serviceTag"
          value={input.serviceTag}
          onChange={handleChange}
        />{" "}
      </TableCell>

      <TableCell>
        {" "}
        <TextField name="date" value={input.date} onChange={handleChange} />
      </TableCell>

      <TableCell>
        <TextField name="user" value={input.user} onChange={handleChange} />
      </TableCell>

      <TableCell>
        <Button
          variant="contained"
          color="secondary"
          onClick={() => changeEditMode()}
        >
          Cancel
        </Button>
      </TableCell>

      <TableCell>
        <Button
          variant="contained"
          color="primary"
          onClick={() => console.log("save")}
        >
          Save
        </Button>
      </TableCell>
    </>
  );
}

export default EditForm;
