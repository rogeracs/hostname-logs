import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import useToggle from "./hooks/useToggle";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import TextField from "@material-ui/core/TextField";
import EditForm from "./EditForm";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 650
  }
}));

export default function HostnameTable({
  record,
  deleteRecord,
  setRecord,
  changeEditMode
}) {
  const classes = useStyles();

  const [SelectedID, SetSelectedID] = useState(-1);

  const editRecord = recordID => {
    const selectedRecord = record.find(log => log.id === recordID);
  };

  const [toggled, setToggle] = useState(false);

  const handleToggle = record => {
    setToggle(!toggled);
  };

  return (
    <div>
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Hostname</TableCell>
              <TableCell align="right">ServiceTag</TableCell>
              <TableCell align="center">Date</TableCell>
              <TableCell align="left">User</TableCell>
              <TableCell />
              <TableCell />
            </TableRow>
          </TableHead>

          <TableBody>
            {record.map(row => (
              <TableRow key={row.id}>
                {toggled ? (
                  <>
                    <TableCell component="th" scope="row">
                      {row.hostname}
                    </TableCell>
                    <TableCell align="right">{row.serviceTag}</TableCell>
                    <TableCell align="center">{row.date}</TableCell>
                    <TableCell align="left">{row.user}</TableCell>

                    <TableCell>
                      <IconButton onClick={() => deleteRecord(row.id)}>
                        <DeleteIcon style={{ color: "red" }} />
                      </IconButton>
                    </TableCell>
                    <TableCell>
                      <IconButton onClick={() => SetSelectedID(row.id)}>
                        <EditIcon style={{ color: "green" }} />
                      </IconButton>
                    </TableCell>
                  </>
                ) : SelectedID === row.id ? (
                  <EditForm
                    id={row.id}
                    hostname={row.hostname}
                    serviceTag={row.serviceTag}
                    date={row.date}
                    user={row.user}
                    // toggle={toggle}
                    setRecord={setRecord}
                    record={record}
                    editRecord={editRecord}
                    handleToggle={handleToggle}
                  />
                ) : (
                  <>
                    <TableCell component="th" scope="row">
                      {row.hostname}
                    </TableCell>
                    <TableCell align="right">{row.serviceTag}</TableCell>
                    <TableCell align="center">{row.date}</TableCell>
                    <TableCell align="left">{row.user}</TableCell>

                    <TableCell>
                      <IconButton onClick={() => deleteRecord(row.id)}>
                        <DeleteIcon style={{ color: "red" }} />
                      </IconButton>
                    </TableCell>
                    <TableCell>
                      <IconButton onClick={() => SetSelectedID(row.id)}>
                        <EditIcon style={{ color: "green" }} />
                      </IconButton>
                    </TableCell>
                  </>
                )}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
}

//----------------------------------------TEST---------------------------------------------------
