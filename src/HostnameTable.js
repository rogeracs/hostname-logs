import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import useToggle from "./hooks/useToggle";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import EditForm from "./EditForm";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 650
  }
}));

export default function HostnameTable({
  record,
  deleteRecord,
  setRecord,
  editRow,
  handleSort,
  header
}) {
  const classes = useStyles();

  const [SelectedID, SetSelectedID] = useState(-1);

  const editRecord = recordID => {
    const selectedRecord = record.find(log => log.id === recordID);
  };

  return (
    <div>
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              {header.map((x, i) => (
                <TableCell key={i}>
                  <div onClick={() => handleSort(x.prop)}>{x.name}</div>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>

          <TableBody>
            {record.map(row => (
              <TableRow key={row.id}>
                <>
                  <TableCell component="th" scope="row">
                    {row.hostname}
                  </TableCell>
                  <TableCell align="right">{row.serviceTag}</TableCell>
                  <TableCell align="center">{row.date}</TableCell>
                  <TableCell align="left">{row.user}</TableCell>

                  <TableCell>
                    <IconButton onClick={() => deleteRecord(row.id)}>
                      <DeleteIcon style={{ color: "red" }} />
                    </IconButton>
                  </TableCell>
                  <TableCell>
                    <IconButton
                      onClick={() => {
                        editRow(row);
                      }}
                    >
                      <EditIcon style={{ color: "green" }} />
                    </IconButton>
                  </TableCell>
                </>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
}
