import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  dense: {
    marginTop: 19
  },
  menu: {
    width: 200
  }
}));

const EditUserForm = ({ currentRecord, updateRecord, setEditing, props }) => {
  const classes = useStyles();

  const [log, setLog] = useState(currentRecord);

  useEffect(() => {
    setLog(currentRecord);
  }, [props]);

  const handleInputChange = event => {
    const { name, value } = event.target;

    setLog({ ...log, [name]: value });
  };

  const handleSubmit = event => {
    event.preventDefault();
    updateRecord(log.id, log);
  };

  return (
    <form>
      <TextField
        className={classes.textField}
        name="hostname"
        value={log.hostname}
        onChange={handleInputChange}
        margin="normal"
      />

      <TextField
        className={classes.textField}
        name="serviceTag"
        value={log.serviceTag}
        onChange={handleInputChange}
        margin="normal"
      />

      <TextField
        className={classes.textField}
        name="date"
        value={log.date}
        onChange={handleInputChange}
        margin="normal"
      />
      <TextField
        className={classes.textField}
        name="user"
        value={log.user}
        onChange={handleInputChange}
        margin="normal"
      />
      <Button
        style={{ marginTop: "20px", marginLeft: "20px" }}
        variant="contained"
        color="primary"
        onClick={handleSubmit}
      >
        Update User
      </Button>

      <Button
        style={{ marginTop: "20px", marginLeft: "20px" }}
        variant="contained"
        color="secondary"
        onClick={() => setEditing(false)}
      >
        Cancel
      </Button>
    </form>
  );
};

export default EditUserForm;
